CC=clang
CFLAGS=-Wall -gdwarf-2
LFLAGS=-L/usr/local/lib
LIBS=-lcrypto -lnanomsg -lpthread
all: node client

node: node.h blockchain.h data_containers/linked_list.h data_containers/dict.h
	$(CC) $(CFLAGS) node.c blockchain.c data_containers/linked_list.c data_containers/dict.c -o node $(LFLAGS) $(LIBS)

client: client.h data_containers/linked_list.h data_containers/dict.h blockchain.h
	$(CC) $(CFLAGS) client.c data_containers/linked_list.c data_containers/dict.c blockchain.c -o client $(LFLAGS) $(LIBS)

clean:
	@rm {node,client}
	@rm {node.core,client.core}
	@rm *.o
